/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1561800293600_7044';

  // add your middleware config here
  config.middleware = [];
  config.security = {
xframe: {
enable: false,
},
csrf: {
enable: false,
ignoreJSON: true, // 默认为 false，当设置为 true 时，将会放过所有 content-type 为 application/json 的请求
useSession: true, // 默认为 false，当设置为 true 时，将会把 csrf token 保存到 Session 中
cookieName: 'csrfToken', // Cookie 中的字段名，默认为 csrfToken
sessionName: 'csrfToken', // Session 中的字段名，默认为 csrfToken
headerName: 'x-csrf-token', // 通过 header 传递 CSRF token 的默认字段为 x-csrf-token
},
// 白名单
    domainWhiteList: ['yibainetwork.com'],
  };
  config.cors = {
    origin:'http://yibainetwork.com',
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS',
    domainWhiteList: ['http://yibainetwork.com'],
  };
  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };

  return {
    ...config,
    ...userConfig,
  };
};
