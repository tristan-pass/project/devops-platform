'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  router.post('/githook', controller.githook.index);
  router.post('/soar', controller.soar.index);
  router.post('/jenkins', controller.jenkins.index);
  router.post('/db_auth', controller.dbAuth.index);
};
