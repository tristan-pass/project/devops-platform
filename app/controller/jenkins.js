'use strict';

const Controller = require('egg').Controller;
const gitlab = require("../component/gitlab");
const jenkins = require("../component/jenkins");
const YAML = require('yamljs');

const NO_MASTER_COMMIT_MESSAGE = "非master分支的提交";
const metadata_filename = "metadata";

class JenkinsController extends Controller {
    async index() {
        const {ctx} = this;
        const uuid = new Date().valueOf();
        const uuid_str = 'ID' + uuid + ": \t";

        try {
            ctx.logger.info(uuid_str + ">>>>>>>>>>>>>>>>一个来访的请求: %j", ctx.request.body);
            if (!require("../auth").isAuth(ctx)) {
                ctx.logger.error(uuid_str + '认证失败的请求,ip: %j' + ctx.ip);
                return;
            }
            if (ctx.request.body.ref != "refs/heads/master") {
                ctx.logger.error(uuid_str + NO_MASTER_COMMIT_MESSAGE);
                ctx.body = NO_MASTER_COMMIT_MESSAGE;
                return;
            }
            let project_id = ctx.request.body.project.id;
            ctx.logger.info(uuid_str + "来访项目为: %j", ctx.request.body.project.homepage);
            // 得到metadata中记录的已经执行的sql的记录
            const projectFileNameList = await gitlab.get_project_file_name_list(project_id);
            // 得到仓库中的sql文件记录
            const metadata_content = await gitlab.get_project_file_content_by_path(project_id, metadata_filename);
            // 得到需要执行的文件
            let new_metadata_content = "---\n";
            // 得到需要执行sql的文件列表
            const record_arr = metadata_content.split("\n");

            let invoke_sql_arr = [];
            for (let item of projectFileNameList) {
                const filepath = item.filepath;
                if (record_arr.indexOf(filepath) < 0 && filepath.endsWith(".jenkins")) {
                    invoke_sql_arr.push(item);
                }
            }
            ctx.logger.info(uuid_str + "需要构建的jenkins job文件为: %j", invoke_sql_arr);

            let is_update;
            for (let item of invoke_sql_arr) {
                // 得到目标mysql数据库
                const file_content = await gitlab.get_project_file_content(project_id, item);

                let file_content_arr = file_content.split("---\n")

                for (let content_item of file_content_arr) {
                    is_update = false;
                    if (content_item == "" || content_item == "---") continue;

                    const first_content_item_index = content_item.indexOf("\n");
                    let target_mysql = content_item.substring('//jenkins:'.length, first_content_item_index);
                    ctx.logger.info(uuid_str + "目标为: %j", target_mysql);

                    let target_mysql_uri = target_mysql.substring(0, target_mysql.indexOf("@"));
                    let target_mysql_db_name = target_mysql.substring(target_mysql.indexOf("@") + 1, target_mysql.length);
                    if (target_mysql == "" || target_mysql_uri == "" || target_mysql_db_name == "") continue;

                    // 连接到目标mysql
                    // 获取连接记录仓库中mysql的数据
                    const connection_content = await gitlab.get_project_file_content_by_path(process.env.GITLAB_CONNECTION_REPOSITORY_ID, "jenkins.yml");
                    let connection_content_obj = YAML.parse(connection_content);// 解析数据
                    const target_info = connection_content_obj[target_mysql_uri];
                    const user = Object.keys(target_info)[0];
                    const password = target_info[user];

                    ctx.logger.info(uuid_str + "开始连接到目标......");
                    const connection = await jenkins.get_jenkins_client(target_mysql_uri, user, password);
                    ctx.logger.info(uuid_str + "成功连接到目标");

                    const detail_sql_content = content_item.substring(first_content_item_index + 1, content_item.length);

                    let jenkins_param = {};
                    const detail_sql_content_arr = detail_sql_content.split("===\n");
                    for (let detail_sql_content_item of detail_sql_content_arr) {
                        if (detail_sql_content_item == "" || detail_sql_content_item.replace("\n", "").trim() == '') continue;
                        const first_index = detail_sql_content_item.indexOf('\n');
                        jenkins_param[detail_sql_content_item.substring(0, first_index)] = detail_sql_content_item.substring(first_index + 1, detail_sql_content_item.length);
                    }
                    const queue_item_number = await jenkins.build_job_with_param(connection,target_mysql_db_name,jenkins_param);
                    is_update = true;
                }
                // 记录开始修改
                new_metadata_content += item.filepath + "\n";
            }
            if (is_update) {
                // 更新仓库中的元数据
                ctx.logger.info(uuid_str + "更新仓库中的元数据: %j", new_metadata_content);
                new_metadata_content = new_metadata_content + metadata_content;
                const update_metadata_result = await gitlab.update_project_file_content(project_id, metadata_filename, new_metadata_content);
                ctx.logger.info(uuid_str + "更新仓库中的元数据的结果为: %j", update_metadata_result.data);
            }
        } catch (e) {
            ctx.logger.error("执行异常: +\n\t\t\t" + e.message + "\n\t\t\t" + e.stack);
        }
        ctx.body = "bad request";
    }
}

module.exports = JenkinsController;
