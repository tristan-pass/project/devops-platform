'use strict';

const Controller = require('egg').Controller;
const YAML = require('yamljs');
const gitlab = require("../component/gitlab");
const mysql = require("../component/mysql");
const sql_analysis = require("../component/sql_analysis");
const server = require("../component/server");

const metadata_filename = "metadata";
const NO_MASTER_COMMIT_MESSAGE = "非master分支的提交";
const mydumper_path = "/data/mysql_bakcup";


class GithookController extends Controller {
    async index() {
        const {ctx} = this;
        const uuid = new Date().valueOf();
        const uuid_str = 'ID' + uuid + ": \t";

        try {
            ctx.logger.info(uuid_str + ">>>>>>>>>>>>>>>>一个来访的请求: %j", ctx.request.body);
            if (!require("../auth").isAuth(ctx)) {
                ctx.logger.error(uuid_str + '认证失败的请求,ip: %j' + ctx.ip);
                return;
            }
            if (ctx.request.body.ref != "refs/heads/master") {
                ctx.logger.error(uuid_str + NO_MASTER_COMMIT_MESSAGE);
                ctx.body = NO_MASTER_COMMIT_MESSAGE;
                return;
            }
            let project_id = ctx.request.body.project.id;
            ctx.logger.info(uuid_str + "来访项目为: %j", ctx.request.body.project.homepage);

            // 得到metadata中记录的已经执行的sql的记录
            const projectFileNameList = await gitlab.get_project_file_name_list(project_id);
            // ctx.logger.info("查询该仓库的sql文件有: %j", projectFileNameList);


            // 得到仓库中的sql文件记录
            const metadata_content = await gitlab.get_project_file_content_by_path(project_id, metadata_filename);
            // ctx.logger.info(uuid_str + "元数据内容为: %j", metadata_content);

            let new_metadata_content = "---\n";
            // 得到需要执行sql的文件列表
            const record_arr = metadata_content.split("\n");

            let invoke_sql_arr = [];
            for (let item of projectFileNameList) {
                const filepath = item.filepath;
                if (record_arr.indexOf(filepath) < 0 && filepath.endsWith(".sql")) {
                    invoke_sql_arr.push(item);
                }
            }
            ctx.logger.info(uuid_str + "需要执行的sql文件为: %j", invoke_sql_arr);

            let is_update;
            for (let item of invoke_sql_arr) {
                // 得到目标mysql数据库
                const file_content = await gitlab.get_project_file_content(project_id, item);
                // ctx.logger.info(uuid_str + "文件内容为 :%j", file_content);

                let file_content_arr = file_content.split("---\n")

                for (let content_item of file_content_arr) {
                    is_update = false;
                    if (content_item == "" || content_item == "---") continue;
                    // ctx.logger.info(uuid_str + "文件中一个子集的内容为: %j", content_item);

                    const first_content_item_index = content_item.indexOf("\n");
                    let target_mysql = content_item.substring(8, first_content_item_index);
                    ctx.logger.info(uuid_str + "目标数据库为: %j", target_mysql);

                    let target_mysql_uri = target_mysql.substring(0, target_mysql.indexOf("@"));
                    let target_mysql_db_name = target_mysql.substring(target_mysql.indexOf("@") + 1, target_mysql.length);
                    const target_mysql_uri_arr = target_mysql_uri.split(":");
                    if (!target_mysql_uri_arr || target_mysql_uri_arr.length < 1) continue;
                    let server_host = target_mysql_uri_arr[0];
                    let mysql_port = target_mysql_uri_arr[1];
                    if (target_mysql == "" || target_mysql_uri == "" || target_mysql_db_name == "") continue;

                    // 连接到目标mysql
                    // 获取连接记录仓库中mysql的数据
                    const connection_content = await gitlab.get_project_file_content_by_path(process.env.GITLAB_CONNECTION_REPOSITORY_ID, "mysql.yml");
                    let connection_content_obj = YAML.parse(connection_content);// 解析数据
                    const user = "root";
                    let password = connection_content_obj[target_mysql_uri][user];
                    // 获取连接记录仓库中服务器的数据
                    const server_connection_content = await gitlab.get_project_file_content_by_path(process.env.GITLAB_CONNECTION_REPOSITORY_ID, "server.yml");
                    let server_connection_content_obj = YAML.parse(server_connection_content);// 解析数据
                    const server_username = "root";
                    let server_password = server_connection_content_obj[server_host][server_username];

                    ctx.logger.info(uuid_str + "开始连接到mysql......");
                    const connection = await mysql.get_connection(server_host, mysql_port, user, password, target_mysql_db_name);
                    ctx.logger.info(uuid_str + "成功连接到mysql");

                    ctx.logger.info(uuid_str + "开始连接到数据库所在的服务器......");
                    const ssh = await server.get_connection(server_host, server_username, server_password);
                    ctx.logger.info(uuid_str + "成功连接到数据库所在的服务器");

                    const detail_sql_content = content_item.substring(first_content_item_index + 1, content_item.length);

                    const detail_sql_content_arr = sql_analysis.split_multi_sql(detail_sql_content);

                    try {
                        for (let detail_sql_content_item of detail_sql_content_arr) {
                            if (detail_sql_content_item == "" || detail_sql_content_item.replace("\n", "").trim() == '') continue;
                            //分析 sql
                            if (sql_analysis.is_disable_sql(detail_sql_content_item)) {
                                ctx.logger.info(uuid_str + "此sql不允许执行 :%j", detail_sql_content_item);
                                continue;
                            }
                            if (sql_analysis.is_danger_sql(detail_sql_content_item)) {
                                // 备份服务上该数据库的该所属数据
                                const fix_dumper_ = sql_analysis.get_fix_dumper(user, target_mysql_db_name, detail_sql_content_item, mydumper_path);

                                ctx.logger.info(uuid_str + "创建备份文件夹 :%j", fix_dumper_.mkdir_command);
                                await server.exec(ssh, fix_dumper_.mkdir_command);
                                ctx.logger.info(uuid_str + "创建备份文件夹成功");

                                try {
                                    ctx.logger.info(uuid_str + "执行备份指令 :%j", fix_dumper_.command);
                                    await server.exec(ssh, 'export MYSQL_PWD=' + password + ' && ' + fix_dumper_.command);
                                    ctx.logger.info(uuid_str + "执行备份指令成功");
                                } catch (e) {
                                    ctx.logger.error(uuid_str + "执行异常: +\n\t\t\t" + e.message + "\n\t\t\t" + e.stack);
                                    if (!e.message.startsWith("mysqldump: Couldn't find table")) {
                                        throw e;
                                    }
                                }
                            }
                            ctx.logger.info(uuid_str + "执行一条具体的sql :%j", detail_sql_content_item);
                            const exec_result = await mysql.exec(connection, detail_sql_content_item);
                            ctx.logger.info(uuid_str + "执行sql的结果 :%j", exec_result);
                            ctx.logger.info("---------------------------------------------------------------------------------------------------------------------------------");
                        }
                    } catch (e) {
                        ctx.logger.error(uuid_str + "执行SQL异常: +\n\t\t\t" + e.message + "\n\t\t\t" + e.stack);
                        await mysql.rollback(connection);
                        throw e;
                    }

                    await mysql.delete_connection(connection);
                    ctx.logger.info(uuid_str + "断开mysql的连接");

                    is_update = true;
                    // ctx.logger.info(uuid_str + "该文件需要更新元数据记录");
                }
                // 记录开始修改
                new_metadata_content += item.filepath + "\n";
                // ctx.logger.info(uuid_str + "新的元数据记录为: %j", new_metadata_content);
            }
            if (is_update) {
                // 更新仓库中的元数据
                ctx.logger.info(uuid_str + "更新仓库中的元数据: %j", new_metadata_content);
                new_metadata_content = new_metadata_content + metadata_content;
                const update_metadata_result = await gitlab.update_project_file_content(project_id, metadata_filename, new_metadata_content);
                ctx.logger.info(uuid_str + "更新仓库中的元数据的结果为: %j", update_metadata_result.data);
            }
        } catch (e) {
            ctx.logger.error(uuid_str + "执行异常: +\n\t\t\t" + e.message + "\n\t\t\t" + e.stack);
        }
        ctx.body = "rest";
    }
}

module.exports = GithookController;
