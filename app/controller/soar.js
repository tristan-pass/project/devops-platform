'use strict';

const Controller = require('egg').Controller;
const soar = require("../component/soar");



class SoarController extends Controller {
    async index() {
        const {ctx} = this;
        try {
            const action_command = ctx.request.body.action_command;
            let original_sql = ctx.request.body.original_sql;
            if (original_sql){
                original_sql = original_sql.replace(/[\n\r]/g,' ');
            }
            const db_info = ctx.request.body.db_info;
            ctx.body =  await soar.action_command(action_command,original_sql,db_info);
            return;
        } catch (e) {
            ctx.logger.error("执行异常: +\n\t\t\t" + e.message + "\n\t\t\t" + e.stack);
        }
        ctx.body = "bad request";
    }
}

module.exports = SoarController;
