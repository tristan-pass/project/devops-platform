'use strict';

const Controller = require('egg').Controller;
const YAML = require('yamljs');
const gitlab = require("../component/gitlab");
const mysql = require("../component/mysql");
const sql_analysis = require("../component/sql_analysis");
const server = require("../component/server");
const util = require('util');

const metadata_filename = "metadata";
const NO_MASTER_COMMIT_MESSAGE = "非master分支的提交";
const mydumper_path = "/data/mysql_bakcup";


class DbAuthController extends Controller {
    async index() {
        const {ctx} = this;
        const uuid = new Date().valueOf();
        const uuid_str = 'ID' + uuid + ": \t";
        debugger
        try {
            ctx.logger.info(uuid_str + ">>>>>>>>>>>>>>>>一个来访的请求: %j", ctx.request.body);
            if (!require("../auth").isAuth(ctx)) {
                ctx.logger.error(uuid_str + '认证失败的请求,ip: %j' + ctx.ip);
                return;
            }
            if (ctx.request.body.ref != "refs/heads/master") {
                ctx.logger.error(uuid_str + NO_MASTER_COMMIT_MESSAGE);
                ctx.body = NO_MASTER_COMMIT_MESSAGE;
                return;
            }
            let project_id = ctx.request.body.project.id;
            ctx.logger.info(uuid_str + "来访项目为: %j", ctx.request.body.project.homepage);

            // 得到metadata中记录的已经执行的sql的记录
            const projectFileNameList = await gitlab.get_project_file_name_list(project_id);
            // 得到仓库中的sql文件记录
            const metadata_content = await gitlab.get_project_file_content_by_path(project_id, metadata_filename);

            let new_metadata_content = "---\n";
            // 得到需要执行sql的文件列表
            const record_arr = metadata_content.split("\n");

            let invoke_sql_arr = [];
            for (let item of projectFileNameList) {
                const filepath = item.filepath;
                if (record_arr.indexOf(filepath) < 0 && filepath.endsWith(".yaml")) {
                    invoke_sql_arr.push(item);
                }
            }
            ctx.logger.info(uuid_str + "需要执行的sql文件为: %j", invoke_sql_arr);

            // 获取连接记录仓库中mysql的数据
            const connection_content = await gitlab.get_project_file_content_by_path(process.env.GITLAB_CONNECTION_REPOSITORY_ID, "mysql.yml");
            let connection_content_obj = YAML.parse(connection_content);// 解析数据

            let is_update;
            for (let item of invoke_sql_arr) {
                // 得到目标mysql数据库
                const file_content = await gitlab.get_project_file_content(project_id, item);
                if (file_content == "") continue;
                let content_item_obj = YAML.parse(file_content);// 解析数据
                for (const mysql_ip_port in content_item_obj) {
                    // 第一层: mysql连接层
                    const connection_content_obj_detail = connection_content_obj[mysql_ip_port];
                    const mysql_user = Object.keys(connection_content_obj_detail)[0];
                    const mysql_password = connection_content_obj_detail[mysql_user];
                    const content_item_obj_1_obj = content_item_obj[mysql_ip_port];
                    // 第二层: 数据库层
                    for (const mysql_db_name in content_item_obj_1_obj) {
                        const content_item_obj_2_obj = content_item_obj_1_obj[mysql_db_name];
                        // 第三层: 客户端用户
                        for (const client_user_name in content_item_obj_2_obj) {
                            const client_password = connection_content_obj_detail[client_user_name];
                            const content_item_obj_3_obj = content_item_obj_2_obj[client_user_name];
                            // 连接到目标mysql
                            const mysql_ip_port_part_arr = mysql_ip_port.split(":");
                            const mysql_ip = mysql_ip_port_part_arr[0];
                            const mysql_port = mysql_ip_port_part_arr[1];
                            ctx.logger.info(uuid_str + "开始连接到mysql......");
                            const mysql_connection = await mysql.get_connection(mysql_ip, mysql_port, mysql_user, mysql_password, mysql_db_name);
                            ctx.logger.info(uuid_str + "成功连接到mysql");
                            // 第四层: 客户端ip
                            for (const client_ip in content_item_obj_3_obj) {
                                // 第五层: 权限
                                const client_auth = content_item_obj_3_obj[client_ip];
                                const db_auth_sql_item = util.format("GRANT %s ON `%s`.* TO '%s'@'%s'  identified by '%s';", client_auth, mysql_db_name, client_user_name, client_ip, client_password);
                                const db_auth_sql_item_log = util.format("GRANT %s ON `%s`.* TO '%s'@'%s'  identified by '%s';", client_auth, mysql_db_name, client_user_name, client_ip, "***");
                                ctx.logger.info(uuid_str + "执行一条具体的sql :%j", db_auth_sql_item_log);
                                const exec_result = await mysql.exec(mysql_connection, db_auth_sql_item);
                                ctx.logger.info(uuid_str + "执行sql的结果 :%j", exec_result);
                                ctx.logger.info("---------------------------------------------------------------------------------------------------------------------------------");
                            }
                            await mysql.delete_connection(mysql_connection);
                            ctx.logger.info(uuid_str + "断开mysql的连接");
                        }
                    }
                    is_update = true;
                }
                // 记录开始修改
                new_metadata_content += item.filepath + "\n";
            }
            if (is_update) {
                // 更新仓库中的元数据
                ctx.logger.info(uuid_str + "更新仓库中的元数据: %j", new_metadata_content);
                new_metadata_content = new_metadata_content + metadata_content;
                const update_metadata_result = await gitlab.update_project_file_content(project_id, metadata_filename, new_metadata_content);
                ctx.logger.info(uuid_str + "更新仓库中的元数据的结果为: %j", update_metadata_result.data);
            }
        } catch (e) {
            ctx.logger.error(uuid_str + "执行异常: +\n\t\t\t" + e.message + "\n\t\t\t" + e.stack);
        }
        ctx.body = "rest";
    }
}

module.exports = DbAuthController;
