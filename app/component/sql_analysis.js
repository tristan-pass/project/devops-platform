const danger_sql_prefix = ['DROP', 'DELETE', 'ALTER'];
const disable_sql_prefix = ['TRUNCATE'];

const char1 = "'".charAt(0);
const char2 = '"'.charAt(0);
const char_sep = ';'.charAt(0);

/**
 * 切割多个sql成数组
 * @param sql
 * @returns {Array}
 */
function split_multi_sql(sql) {
    const sql_arr = [];
    let char1_num = 0;
    let char2_num = 0;
    let left_position = 0;

    for (let i = 0; i < sql.length; i++) {
        const sql_char = sql.charAt(i);
        if (sql_char == char1) {
            if (char2_num != 0) {
                if (char1_num == 0) {
                    char1_num = 1;
                } else if (char1_num == 1) {
                    char1_num = 0;
                }
            }
        } else if (sql_char == char2) {
            if (char1_num != 0) {
                if (char2_num == 0) {
                    char2_num = 1;
                } else if (char2_num == 1) {
                    char2_num = 0;
                }
            }
        } else if (sql_char == char_sep) {
            if (char1_num == 0 && char2_num == 0) {
                sql_arr.push(sql.substring(left_position, i));
                left_position = i + 1;
            }
        }
        if (i == sql.length - 1 && left_position < sql.length) {
            sql_arr.push(sql.substring(left_position, sql.length));
        }
    }

    return sql_arr;
}

/**
 * 是否是禁用的sql
 * @param sql
 * @returns {boolean}
 */
function is_disable_sql(sql) {
    const sql_ = sql.trim().toUpperCase();
    for (let item of disable_sql_prefix) {
        if (sql_.startsWith(item)) return true;
    }
    return false;
}

/**
 * 是否是危险sql
 * @param sql
 * @returns {boolean} 是/否
 */
function is_danger_sql(sql) {
    const sql_ = sql.trim().toUpperCase();
    for (let item of danger_sql_prefix) {
        if (sql_.startsWith(item)) return true;
    }
    return false;
}

/**
 * 判断是否以执行字符开头
 * @param original_str
 * @param start_str
 * @returns {boolean}
 */
function startsWith(original_str, start_str) {
    const start_str_length = start_str.length;
    if (start_str_length <= original_str.length) {
        const finded_str = original_str.substring(0, start_str_length);
        if (finded_str.toUpperCase() == start_str) {
            return true;
        }
    }
    return false;
}

const DROP_STR = 'DROP';
const DROP_STR_LENGTH = DROP_STR.length;

const IF_STR = 'IF';
const IF_STR_LENGTH = IF_STR.length;

const EXISTS_STR = 'EXISTS';
const EXISTS_STR_LENGTH = EXISTS_STR.length;


const DATABASE_STR = 'DATABASE';

const TABLE_STR = 'TABLE';
const TABLE_STR_LENGTH = TABLE_STR.length;

const DELETE_STR = 'DELETE';
const DELETE_STR_LENGTH = DELETE_STR.length;

const FROM_STR = 'FROM';
const FROM_STR_LENGTH = FROM_STR.length;

const WHERE_STR = 'WHERE';
const WHERE_STR_LENGTH = WHERE_STR.length;

const TRUNCATE_STR = 'TRUNCATE';
const TRUNCATE_STR_LENGTH = TRUNCATE_STR.length;

const ALTER_STR = 'ALTER';
const ALTER_STR_LENGTH = ALTER_STR.length;


/**
 * 分析得到DDL语句的补偿 的 mydumper语法
 // mysqldump -u用户名 -p密码 数据库名 表名 --where="筛选条件" > 导出文件路径
 * @param sql
 */
function get_fix_dumper(user, target_mysql_db_name, original_sql, mydumper_path) {
    let command = '/usr/local/mysql/bin/mysqldump -u' + user + ' ' + target_mysql_db_name + ' ';
    original_sql = original_sql.trim();
    if (startsWith(original_sql, DROP_STR)) {
        original_sql = original_sql.substring(DROP_STR_LENGTH, original_sql.length).trim();
        if (startsWith(original_sql, DATABASE_STR)) {
        } else if (startsWith(original_sql, TABLE_STR)) {
            original_sql = original_sql.substring(TABLE_STR_LENGTH, original_sql.length).trim();
            if (startsWith(original_sql, IF_STR)) {
                original_sql = original_sql.substring(IF_STR_LENGTH, original_sql.length).trim();
                if (startsWith(original_sql, EXISTS_STR)) {
                    original_sql = original_sql.substring(EXISTS_STR_LENGTH, original_sql.length).trim();
                    original_sql = original_sql.replace(/`/g, '');
                    command += original_sql;
                }
            }
        }
    } else if (startsWith(original_sql, DELETE_STR)) {
        original_sql = original_sql.substring(DELETE_STR_LENGTH, original_sql.length).trim();
        original_sql = original_sql.substring(FROM_STR_LENGTH, original_sql.length).trim();
        original_sql = original_sql.replace(/`/g, '');
        const table_name_index = original_sql.indexOf(' ');
        const table_name = original_sql.substring(0, table_name_index).trim();
        command += table_name + ' ';
        command += ' --no-create-info ';
        // 看是否有条件语句
        original_sql = original_sql.substring(table_name_index, original_sql.length).trim();
        if (startsWith(original_sql, WHERE_STR)) {
            original_sql = original_sql.substring(WHERE_STR_LENGTH, original_sql.length).trim();
            command += ' --where="' + original_sql + '" ';
        }
    } else if (startsWith(original_sql, ALTER_STR)) {
        original_sql = original_sql.substring(ALTER_STR_LENGTH, original_sql.length).trim();
        if (startsWith(original_sql, TABLE_STR)) {
            original_sql = original_sql.substring(TABLE_STR_LENGTH, original_sql.length).trim();
            original_sql = original_sql.replace(/`/g, '');
            const table_name_index = original_sql.indexOf(' ');
            const table_name = original_sql.substring(0, table_name_index).trim();
            command += table_name + ' ';
            // command += ' --no-data ';
        }
    } else if (startsWith(original_sql, TRUNCATE_STR)) {
        original_sql = original_sql.substring(TRUNCATE_STR_LENGTH, original_sql.length).trim();
        if (startsWith(original_sql, DATABASE_STR)) {
        } else if (startsWith(original_sql, TABLE_STR)) {
            original_sql = original_sql.substring(TABLE_STR_LENGTH, original_sql.length).trim();
            if (startsWith(original_sql, IF_STR)) {
                original_sql = original_sql.substring(IF_STR_LENGTH, original_sql.length).trim();
                if (startsWith(original_sql, EXISTS_STR)) {
                    original_sql = original_sql.substring(EXISTS_STR_LENGTH, original_sql.length).trim();
                    original_sql = original_sql.replace(/`/g, '');
                    command += original_sql;
                }
            }
        }
    }
    // 得到导出的文件存放位置
    const cur_date = new Date();
    const cur_date_str = cur_date.getFullYear() + '/' + cur_date.getMonth() + '/' + cur_date.getDate();
    const dumper_dir_path = mydumper_path + '/' + cur_date_str;
    command += ' |gzip > ' + dumper_dir_path + '/' + cur_date.getHours() + cur_date.getMinutes() + cur_date.getSeconds() + cur_date.getMilliseconds() + '-' + target_mysql_db_name + '.sql.gz';
    const mkdir_command = 'mkdir -p ' + dumper_dir_path;
    return {mkdir_command, command};
}

module.exports = {
    is_disable_sql: is_disable_sql,
    is_danger_sql: is_danger_sql,
    get_fix_dumper: get_fix_dumper,
    split_multi_sql: split_multi_sql,
}