const jenkins_lib = require('jenkins');
const util = require('util');

/**
 *
 * @param jenkins
 * @param job_name
 * @param param
 * @returns {Promise<any>} queue_item_number
 */
async function build_job_with_param(jenkins, job_name, param) {
    const build_info = {};
    build_info.name = job_name;
    if (param && Object.keys(param).length > 0) {
        build_info.parameters = param;
    }

    return await new Promise(function (resolve, reject) {
        jenkins.job.build(build_info, function (err,data) {
            if (err) {
                reject(err);
                return;
            }
            resolve(data);
        });
    })
}

/**
 * 获取服务信息
 * @param jenkins
 * @returns {Promise<any>}
 */
async function get_info(jenkins) {
    return await new Promise(function (resolve, reject) {
        jenkins.info(function (err, data) {
            if (err) {
                reject(err);
                return;
            }
            resolve(data);
        })
    })
}

/**
 * 获取jenkins客户端连接
 * @param jenkins_base_url
 * @param username
 * @param password
 * @returns {Promise<*>}
 */
async function get_jenkins_client(jenkins_base_url, username, password) {
    const jenkins = jenkins_lib({
        baseUrl: util.format('http://%s:%s@%s', username, password, jenkins_base_url),
        crumbIssuer: true
    });
    return jenkins;
}


module.exports = {
    get_jenkins_client,
    get_info,
    build_job_with_param

}