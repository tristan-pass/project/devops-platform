node_ssh = require('node-ssh')

async function get_connection(host, username, password) {
    const ssh = new node_ssh();
    await ssh.connect({
        host: host,
        username: username,
        port: 22,
        password,
        tryKeyboard: true,
        onKeyboardInteractive: (name, instructions, instructionsLang, prompts, finish) => {
            if (prompts.length > 0 && prompts[0].prompt.toLowerCase().includes('password')) {
                finish([password])
            }
        }
    });
    return ssh;
}

/**
 * 执行指令
 * @param ssh
 * @param command
 * @returns {Promise<void>}
 */
async function exec(ssh, command) {
    return await ssh.exec(command);
}

module.exports = {
    exec: exec,
    get_connection: get_connection,
}