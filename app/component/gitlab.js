/*
参考文档:

https://github.com/gitlabhq/gitlabhq/tree/master/doc/api
https://github.com/gitlabhq/gitlabhq/blob/master/doc/api/repositories.md
https://github.com/gitlabhq/gitlabhq/blob/master/doc/api/repository_files.md
 */
const util = require('util');
const axios = require('axios');
axios.defaults.baseURL = process.env.GITLAB_API_URL;
axios.defaults.headers.common['PRIVATE-TOKEN'] = process.env.GITLAB_ACCESS_TOKEN;

const MASTER = "master";


/**
 * 获取项目中文件的名称列表
 * @param project_id
 */
async function get_project_file_name_list(project_id) {
    let result = [];
    const url = util.format("/projects/%s/repository/tree", project_id);
    try {
        let data = await axios.get(url,
            {params: {ref: MASTER, recursive: true, per_page: 999999}}
        );
        for (let item of data.data) {
            if (item.type == "tree") continue;
            result.push({filepath: item.path, id: item.id})
        }
    } catch (error) {
        console.error(error);
    }
    return result;
}

/**
 * 获取项目中文件的内容
 * @param project_id
 * @param file_path
 */
async function get_project_file_content(project_id, fileinfo) {
    let content = "";
    if (!project_id || !fileinfo || !fileinfo.id) return content;
    const url = util.format("/projects/%s/repository/blobs/%s/raw", project_id, fileinfo.id);
    try {
        let data = await axios.get(url, {params: {ref: MASTER}});
        content = data.data;
    } catch (error) {
        console.error(error);
    }
    return content;
}

/**
 * 通过文件路径获取项目中文件的内容
 * @param project_id
 * @param file_path
 */
async function get_project_file_content_by_path(project_id, file_path) {
    let content = "";
    if (!project_id || !file_path) return content;
    const url = util.format("/projects/%s/repository/files/%s/raw", project_id, file_path);
    try {
        let data = await axios.get(url, {params: {ref: MASTER}});
        content = data.data;
    } catch (error) {
        console.error(error);
    }
    return content;
}

/**
 * 修改项目中文件的内容
 * @param project_id
 * @param file_path
 */
async function update_project_file_content(project_id, file_path, content) {
    const url = util.format("/projects/%s/repository/files/%s", project_id, file_path);
    let data;
    try {
        const author_email = "robot@tristan.com";
        const author_name = "robot";
        const commit_message = "robot commit file";

        data = await axios.put(url, {
                branch: MASTER,
                author_email: author_email,
                author_name: author_name,
                content: content,
                commit_message: commit_message
            }
        );
    } catch (error) {
        console.error(error);
    }
    return data;
}


module.exports = {
    get_project_file_name_list,
    get_project_file_content,
    get_project_file_content_by_path,
    update_project_file_content,

}