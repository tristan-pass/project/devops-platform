const mysql = require('mysql');

async function rollback(connection) {
    await new Promise(function (resolve,) {
        connection.rollback(function () {
            resolve(1);
        });
    })
}

/**
 * 获取连接
 */
async function get_connection(host, port, user, password, db) {
    return await new Promise(function (resolve, reject) {
        const connection = mysql.createConnection({
            host: host,
            port: port,
            user: user,
            password: password,
            database: db
        });
        connection.connect(function (error) {
            if (error) {
                reject(error);
                console.log(error);
            }
        });
        resolve(connection);
    })
}

/**
 * 删除连接
 * @param connection
 */
async function delete_connection(connection) {
    return await new Promise(function (resolve, reject) {
        connection.end(function (error) {
            if (error) {
                reject(error);
                console.log(error);
            }
            resolve(1);
        });
    })
}

/**
 * 执行sql
 */
async function exec(connection, sql) {
    sql = sql.trim();
    if (!sql || sql == "" || sql == "\n") return;
    sql = sql + ";";
    return await new Promise(function (resolve, reject) {
        connection.query(sql, function (error, results, fields) {
            if (error) {
                console.log(error);
                reject(error);
            }
            resolve(results);
        });
    });
}

module.exports = {
    exec,
    get_connection,
    delete_connection,
    rollback
}