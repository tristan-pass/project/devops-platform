const shell = require('shelljs');
const util = require('util');

async function action_command(action_command, sql, db_info) {
    if (!shell.which('soar')) {
        result = "soar不可用,请联系管理员";
        shell.exit(1);
    }
    switch (action_command) {
        case "analysis_sql":
            return analysis_sql(sql, db_info);
        case "check_sql":
            return check_sql(sql, db_info);
        case "format_sql":
            return format_sql(sql, db_info);
        case "refactor_sql":
            return refactor_sql(sql, db_info);
    }
}

function fix_soar_db_info(db_info) {
    let result = '';
    if (db_info && db_info.db_ip && db_info.db_port && db_info.db_username && db_info.db_password && db_info.db_schema) {
        result = util.format('-test-dsn="%s:%s@%s:%s/%s" -allow-online-as-test -log-output=/logs/soar.log', db_info.db_ip, db_info.db_port, db_info.db_username, db_info.db_password, db_info.db_schema);
    }
    return result;
}

/**
 * 分析SQL 语法
 */
async function analysis_sql(sql, db_info) {
    const fix_db_info = fix_soar_db_info(db_info);
    const command = util.format("echo '%s' | soar %s", sql, fix_db_info);
    const command_result = shell.exec(command);
    return command_result;
}

/**
 * 检查SQL 语法
 */
async function check_sql(sql, db_info) {
    const fix_db_info = fix_soar_db_info(db_info);
    const command = util.format("echo '%s' | soar %s  -only-syntax-check", sql, fix_db_info);
    const command_result = shell.exec(command);
    return command_result;
}

/**
 * 格式化SQL
 */
async function format_sql(sql, db_info) {
    const fix_db_info = fix_soar_db_info(db_info);
    const command = util.format("echo '%s' | soar %s pretty", sql, fix_db_info);
    const command_result = shell.exec(command);
    return command_result;
}

/**
 * 重构sql
 * @param sql
 */
async function refactor_sql(sql, db_info) {
    const fix_db_info = fix_soar_db_info(db_info);
    const command = util.format("echo '%s' | soar %s -rewrite-rules star2columns,delimiter rewrite", sql, fix_db_info);
    const command_result = shell.exec(command);
    return command_result;
}

module.exports = {
    action_command,
}