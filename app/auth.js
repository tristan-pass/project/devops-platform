/**
 * 是否认证
 * @param ctx context信息
 * @returns {boolean}
 */
function isAuth(ctx) {
    let flag = true;
    // 通过ip去放行
    flag = allow_auth_by_iplist(ctx);
    // 通过gitlab登录去放行

    return flag;
}

/**
 * 通过判断ip 列表去判断是否认证
 * @param ctx
 * @returns {boolean}
 */
function allow_auth_by_iplist(ctx) {
    let flag = true;
    let allow_ip_list = [];
    const allow_ip_str_list = process.env.ALLOW_IP_STR_LIST;
    if (allow_ip_str_list) {
        allow_ip_list = allow_ip_str_list.split(",");
        if (!allow_ip_list) {
            allow_ip_list = [];
        }
    }
    let host_ip = ctx.ip;
    if (allow_ip_list.indexOf(host_ip)<0) flag = false;
    if (!flag) {
        ctx.status = 401;
        ctx.body = "授权已过期,请重新授权"
    }
    return flag;
}



module.exports = {
    isAuth: isAuth
}